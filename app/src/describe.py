#!/usr/bin/env python
"""The describe functionality."""
from typing import List


def describe() -> List[str]:
    """Is a dummy.

    Take: no arguments
    Return: list of words.
    """
    # TODO: take args
    # TODO: ping api
    # TODO: get "describe"
    # TODO: return it as list
    return ['dummy', 'info', 'describing', 'keyword']
