#!/usr/bin/env python
"""Changepoint analysis by binary segmentation.

paper: Taylor, Wayne A - Change-Point Analysis:A Powerful New Tool For Detecting Changes
"""
from typing import Dict

from pandas import DataFrame, concat  # type: ignore
from altair import Chart  # type: ignore
from tqdm import tqdm  # type: ignore


class BaseData:
    """Give a time series w indices called `key` and data called `doc_count`.

    Indices are in epoch time * 1000.
    """

    def __init__(self, dat: DataFrame):
        """Init BaseData."""
        self.dataframe = dat
        self.chart = Chart(self.dataframe).mark_line().encode(x='key:T', y='doc_count')
        self.values = self.dataframe.doc_count.values
        self.cusum0 = self.cusum(self.dataframe)
        self.sdiff0 = self.sdiff(self.cusum0)
        self.cusum_chart = Chart(
            DataFrame(self.cusum0.items())).mark_line().encode(x='0:T', y='1:Q')
        # an initial estimate of the changepoint
        self.changepoint = max(self.cusum0.items(), key=lambda x: abs(x[1]))[0]

    @staticmethod
    def cusum(dat: DataFrame) -> Dict[int, float]:
        """Produce a cumulative sum of residuals.

        cusums[0] = 0
        cusums[n+1] = cusums[n] + (X[n+1] - X.mean())
        """
        xbar = dat.doc_count.mean()
        cusums: Dict[int, float] = dict()
        prev = dat.key.values[0] - 86400000
        cusums[prev] = 0
        for row in dat.values:
            epoch_key = row[0]
            doc_count = row[1]
            resid = doc_count - xbar
            cusums[epoch_key] = cusums[prev] + resid
            prev = epoch_key
        return cusums

    @staticmethod
    def sdiff(cusum: Dict[int, float]) -> float:
        """Take the distance between max and min of cusum.

        cusum.max() - cusum.min().
        """
        dat = DataFrame(
            cusum.items()).rename(
                columns={0: 'index', 1: 'cumulative_sum'}
            )
        return dat.cumulative_sum.max() - dat.cumulative_sum.min()


class BinarySegmentation(BaseData):
    """Do binary segmentation once to find one changepoint and two segments."""

    def __init__(self, dat: DataFrame, num_bootstraps: int):
        """Initialize BinarySegmentation.

        If bootstraps return confidence that a changepoint is appropriate, add labels to dataframe.
        """
        super().__init__(dat)
        self.bootstrap_counter = 0
        self.num_bootstraps = num_bootstraps
        self.confidence = self.do_bootstrap()
        if self.confidence > 0.95:
            self.changepoint = self.minimize_mse()
            self.dataframe = self.dataframe.assign(label=self.label().label)
            self.chart = Chart(self.dataframe).mark_line().encode(
                x='key:T',
                y='doc_count:Q',
                color='label:N'
            )

        else:
            print('we are not confident that cpa is relevant (bootstrapconfidence <= 0.95)')

    def bootstrap(self) -> None:
        """Sample the time series order to check changes in the cumulative sum."""
        btstrp = self.dataframe.sample(frac=1)
        sdiff1 = self.sdiff(self.cusum(btstrp))
        if sdiff1 < self.sdiff0:
            self.bootstrap_counter += 1

    def do_bootstrap(self) -> float:
        """Do the specified number of samples and return confidence."""
        for _ in tqdm(range(self.num_bootstraps), 'performing bootstrap'):
            self.bootstrap()

        return self.bootstrap_counter / self.num_bootstraps

    def modified_mse(self, changepoint: int) -> float:
        """Squared error modified for a changepoint."""
        x_left = self.dataframe.doc_count[self.dataframe.key <= changepoint]
        x_right = self.dataframe.doc_count[self.dataframe.key > changepoint]
        xbar_left = x_left.mean()
        xbar_right = x_right.mean()

        return ((x_left - xbar_left)**2).sum() + ((x_right - xbar_right)**2).sum()

    def minimize_mse(self) -> int:
        """Brute force argmin."""
        curr_min: float = 2**64
        curr_argmin: int = 0
        for key in tqdm(self.dataframe.key, desc='minimizing MSE'):
            mse = self.modified_mse(key)
            if mse < curr_min:
                curr_min = mse
                curr_argmin = key
        return curr_argmin

    def label(self) -> DataFrame:
        """Label the dataframe with your segments.

        To be done after minimize_mse set self.changepoint to the correct value.
        """
        left = self.dataframe[self.dataframe.key <= self.changepoint].assign(
            label=lambda x: 'left'
        )
        right = self.dataframe[self.dataframe.key > self.changepoint].assign(
            label=lambda x: 'right'
        )

        return concat((left, right))
