#!/usr/bin/env python
"""Timeline functionality."""


from collections import defaultdict
from datetime import datetime
import json
from typing import Dict, Any, List, Tuple, Optional

import requests
from requests.models import Response
from pandas import DataFrame  # type: ignore
from altair import Chart  # type: ignore
from numpy import log  # type: ignore

from ..settings import REDDIT_COMMENT_SEARCH, TWITTER_VERIFIED_SEARCH
from .timeline_changepoint import BinarySegmentation


def agg(
        term: str,
        reddit_or_twitter: str = 'reddit',  # Literal['twitter', 'reddit']
        interval: str = 'day',  # Literal['hour', 'day', 'week', 'month']
        since: Optional[Tuple[int, int, int]] = None,
        until: Optional[Tuple[int, int, int]] = None
        ) -> Tuple[Response, str]:
    """Elastic search aggregation."""
    headers = {'Content-Type': 'application/json',
               'Accept-Encoding': 'deflate, compress, gzip'}

    if not since:
        since = (2019, 1, 1)

    if reddit_or_twitter == 'reddit':
        url = REDDIT_COMMENT_SEARCH
        created = 'created_utc'
        content = 'body'
    elif reddit_or_twitter == 'twitter':
        url = TWITTER_VERIFIED_SEARCH
        created = 'created_at'
        content = 'text'

    def dt_to_epoch(date: datetime) -> str:
        """Datetime object to epoch time."""
        return date.strftime('%s')

    query: Dict[str, Any] = defaultdict(
        lambda: defaultdict(
            lambda: defaultdict(
                lambda: defaultdict(dict))))

    query['query']['bool']['must'] = list()
    query['aggs'][created]['date_histogram']['field'] = created
    query['aggs'][created]['date_histogram']['order']['_key'] = 'asc'
    query['aggs'][created]['date_histogram']['interval'] = interval

    query['query']['bool']['must'].append({'match': {content: term}})
    _since = dt_to_epoch(datetime(*since))
    query['query']['bool']['must'].append({'range': {created: {'gt': _since}}})

    if until:
        _until = dt_to_epoch(datetime(*until))
    else:
        _until = dt_to_epoch(datetime.today())
    query['query']['bool']['must'].append({'range': {created: {'lt': _until}}})

    response = requests.get(str(url),
                            data=json.dumps(query),
                            headers=headers)

    if response.status_code == 200:
        assert response.json()['aggregations'][created]['buckets'], \
            'query returned an empty list'
        return response, created
    raise Exception('timeline aggregation response: not 200')


def mk_df(response: Response, created: str = 'reddit') -> DataFrame:
    """Take a 200 response and make a dataframe."""
    try:
        content = response.json()['aggregations'][created]['buckets']
    except Exception as exception:
        raise exception

    for i in content:
        del i['key_as_string']

    def transform(inp: List[Dict[str, int]]) -> Dict[str, List[int]]:
        out: Dict[str, List[int]] = defaultdict(list)
        for i in inp:
            for key, val in i.items():
                out[key].append(val)

        return out

    dat = DataFrame(transform(content))

    return dat.assign(
        date=dat.key.apply(lambda x: datetime.fromtimestamp(x//1000))
    )


def mk_chart(dat: DataFrame) -> Chart:
    """Make a chart."""
    chart = Chart(dat, width=500).mark_line().interactive()

    return chart.encode(
        x='date:T',
        y='doc_count:Q'
    )


def timeline(
        term: str,
        reddit_or_twitter: str = 'reddit',
        interval: str = 'day',
        since: Optional[Tuple[int, int, int]] = (2019, 1, 1),
        until: Optional[Tuple[int, int, int]] = None,
        do_changepoint: bool = True
) -> Tuple[DataFrame, Chart, Optional[datetime]]:
    """Return a dataframe and a chart."""
    response, created = agg(
        term=term, reddit_or_twitter=reddit_or_twitter, interval=interval, since=since, until=until
    )

    dat = mk_df(response, created)
    chart = mk_chart(dat)
    changepoint = None
    if do_changepoint:
        changepoint_analysis = BinarySegmentation(
            dat,
            max(int(dat.shape[0] * log(dat.shape[0]) / 8), dat.shape[0])
        )
        dat = changepoint_analysis.dataframe
        chart = changepoint_analysis.chart
        _changepoint = changepoint_analysis.changepoint
        changepoint = datetime.fromtimestamp(_changepoint//1000)
    return dat, chart, changepoint
