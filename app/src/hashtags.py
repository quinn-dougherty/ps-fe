#!/usr/bin/env python
"""Related Hashtags functionality."""

import heapq
from collections import defaultdict
import pandas as pd  # type: ignore
import altair as alt  # type: ignore
import requests


def hashtag_query_es(term: str, limit: int):
    """Query the api for tweets."""
    headers = {'Content-type': 'application/json'}
    response = requests.get(('https://twitter-es.pushshift.io/twitter_verified/_search/?q=text:' +
                             term + '&sort=created_at:desc&size=' + str(limit)), headers=headers)

    # print(json.dumps(result, indent=1))
    if response.status_code == 200:
        return response.json()
    raise Exception('Hashtag API response: not 200')


def mk_df(result, num_of_hashtags_graphed):  # -> Chart
    """Make a dataframe from results."""
    hashtag_dict = defaultdict(int)
    if 'hits' in result:
        for obj in result['hits']['hits']:
            hashtags_obj = obj['_source']['entities']['hashtags']
            if hashtags_obj:
                for hashtag_obj in hashtags_obj:
                    hashtag = hashtag_obj['text']
                    hashtag_dict[hashtag] += 1
    top_hashtags = heapq.nlargest(
        int(num_of_hashtags_graphed),
        hashtag_dict,
        key=hashtag_dict.get)  # gets highest hashtags

    hashtags_ranked = {}  # makes dictionary of just highest ones
    for hashtag in top_hashtags:
        hashtags_ranked[hashtag] = hashtag_dict[hashtag]

    dataframe = pd.DataFrame.from_dict(hashtags_ranked,
                                       orient='index',
                                       columns=['Hashtag Frequency'])
    return dataframe


def mk_chart(dataframe, term):  # : DataFrame #-> Chart
    """Make a chart."""
    return alt.Chart(dataframe.reset_index(),
                     title=("Hashtags Occuring with " + term)).mark_bar().encode(
                         x='sum(Hashtag Frequency):Q',
                         y=alt.Y(
                             'index:N',
                             sort=alt.EncodingSortField(
                                 field='Hashtag Frequency',  # The field to use for the sort
                                 op="sum",  # The operation to run on the field prior to sorting
                                 order="descending"),  # The order to sort in
                             title='Hashtags'))


def hashtags(term: str,
             limit: int,
             num_of_hashtags_graphed: int):  # -> Tuple[DataFrame, Chart]
    """Return a dataframe and chart of related hashtags."""
    result = hashtag_query_es(term, limit)
    dataframe = mk_df(result, num_of_hashtags_graphed)
    chart = mk_chart(dataframe, term)
    return dataframe, chart
