#!/usr/bin/env python
"""Activity is a bar chart."""

from altair import Chart  # type: ignore
from pandas import DataFrame  # type: ignore


def activity() -> Chart:
    """Take query args and return a bar chart.

    Each of these TODOs can be their own function.
    """
    # TODO: take arguments from user/route
    # TODO: ping api
    # TODO: wrangle the json into a dataframe
    # TODO: make a chart

    # dummy data
    source = DataFrame({
        'a': ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'],
        'b': [28, 55, 43, 91, 81, 53, 19, 87, 52]
    })

    dummy = Chart(source).mark_bar().encode(
        x='a',
        y='b'
    )
    return dummy
