#!/usr/bin/env python

from app import APP
from unittest import TestCase


class MainTest(TestCase):
    """Tests the main route."""
    def setUp(self) -> None:
        """Runs before the test."""
        APP.config['TESTING'] = True
        self.APP = APP.test_client()

    def test_main(self) -> None:
        """Verifies that home route returns 200."""
        response = self.APP.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
