#!/usr/bin/env python

from app import APP
from unittest import TestCase
import os
from shutil import rmtree


class TimelineTest(TestCase):
    """Tests the timeline functionality

    ROUTES: /timeline
    """
    def setUp(self) -> None:
        """Runs before the test."""
        BASE_PATH = os.path.dirname(os.path.abspath(__file__))
        DOWNLOAD_FOLDER = BASE_PATH + '/downloads/'
        TMP_FOLDER = BASE_PATH + '/tmp/'
        os.makedirs(DOWNLOAD_FOLDER, exist_ok=True)
        os.makedirs(TMP_FOLDER, exist_ok=True)

        APP.config['TESTING'] = True
        APP.config['WTF_CSRF_ENABLED'] = False

        self.APP = APP.test_client()

    def test_timeline_reddit(self) -> None:
        """Verifies that the /timeline is returning 200 when pinging reddit."""
        test_case = {
            'term': 'tornado',
            'reddit_or_twitter': 'reddit',
            'interval': 'month',
            'since': '2019-11-01',
            'until': '',
            'changepoint': 'False'
        }
        response = self.APP.post('/timeline', data=test_case)
        self.assertEqual(response.status_code, 200)

    def test_timeline_twitter(self) -> None:
        """Verifies that the /timeline is returning 200 when pinging twitter."""
        test_case = {
            'term': '(tornado|tornadoes)',
            'reddit_or_twitter': 'twitter',
            'interval': 'week',
            'since': 'fish',
            'until': '2019-04-01',
            'changepoint': 'True'
        }
        response = self.APP.post('/timeline', data=test_case)
        self.assertEqual(response.status_code, 200)


class TimelineTest_Chart_Download(TestCase):
    """Tests the timeline chart and download.

    ROUTES:  /timeline/chart, /timeline/download/<filename>
    """

    def setUp(self) -> None:
        """Runs before test."""
        APP.config['TESTING'] = True
        APP.config['WTF_CSRF_ENABLED'] = False

        self.APP = APP.test_client()

    def test_timeline_chart(self) -> None:
        """Verifies that timeline/chart is returning a 200."""
        response = self.APP.get('/timeline/chart')
        self.assertEqual(response.status_code, 200)

    def test_timeline_download(self) -> None:
        """Verifies that timeline/download/filename is returning a 200."""
        # response = self.APP.get('/timeline/download/tornado-reddit-bymonth.csv')
        term = '(tornado|tornadoes)'
        response = self.APP.get(f'/timeline/download/{term}-twitter-byweek.csv')
        self.assertEqual(response.status_code, 200)

#   def tearDown(self) -> None:
#       """Runs after the tests"""
#       BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#       DOWNLOAD_FOLDER = BASE_PATH + '/downloads/'
#       TMP_FOLDER = BASE_PATH + '/tmp/'
#
#       rmtree(DOWNLOAD_FOLDER)
#       rmtree(TMP_FOLDER)
#
