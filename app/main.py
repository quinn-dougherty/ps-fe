#!/usr/bin/env python
"""The main application logic."""

import os
import json
# import sys
# import logging
import re
from typing import Tuple, Optional
# from datetime import datetime

from flask import (Flask,  # type: ignore
                   render_template,
                   request,
                   send_from_directory)
from flask.wrappers import Response  # type: ignore
from flask.logging import create_logger  # type: ignore

from .src.wordcloud import wordcloud
from .src.activity import activity
from .src.timeline import timeline
from .src.describe import describe
from .src.hashtags import hashtags

APP = Flask(__name__)

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
DOWNLOAD_FOLDER = BASE_PATH + '/downloads/'
TMP_FOLDER = BASE_PATH + '/tmp/'
os.makedirs(DOWNLOAD_FOLDER, exist_ok=True)
os.makedirs(TMP_FOLDER, exist_ok=True)
APP.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER
APP.config['TMP_FOLDER'] = TMP_FOLDER

LOG = create_logger(APP)


@APP.route('/')
def main() -> str:
    """Render index.html."""
    return render_template('index.html', title='Social Media Analysis Toolkit')


########################################
#              activity           ######
########################################
@APP.route('/activity', methods=['POST'])
def _activity() -> str:
    """Render the activity template."""
    return render_template('activity.html')


@APP.route('/activity/chart')
def _activity_chart() -> str:
    """Serve the activity chart json."""
    # TODO: get arguments from user
    # TODO: pass those arguments into activity()
    return activity().interactive().to_json()


########################################
#              timeline           ######
########################################
@APP.route('/timeline', methods=['POST'])
def _timeline() -> str:
    """Render the timeline template."""
    def mk_since_until(date: str) -> Optional[Tuple[int, int, int]]:
        """Transform input into needed format.

        if input is missing or incorrect, return None.
        """
        rpat = r'\d\d\d\d[-/]\d\d[-/]\d\d'
        if date and re.match(rpat, date):
            return int(date[:4]), int(date[5:7]), int(date[8:])
        return None

    term = request.values['term'] or 'tornado'
    reddit_or_twitter = request.values['reddit_or_twitter']
    interval = request.values['interval']
    since = request.values['since']  # returns a string in 'YYYY-MM-DD' format
    until = request.values['until']  # returns a string in 'YYYY-MM-DD' format
    _do_changepoint = request.values['changepoint']  # returns a string 'True' or 'False'
    do_changepoint: bool = _do_changepoint == 'True'

    since_ = mk_since_until(since)
    until_ = mk_since_until(until)

    dat, chart, changepoint = timeline(
        term=term, reddit_or_twitter=reddit_or_twitter,
        interval=interval, since=since_, until=until_,
        do_changepoint=do_changepoint
    )

    with open(APP.config['TMP_FOLDER'] + 'chart.json', 'w') as chart_json_out:
        json.dump(chart.to_json(), chart_json_out)

    filename = f'{term}-{reddit_or_twitter}-by{interval}.csv'
    dat.to_csv(APP.config['DOWNLOAD_FOLDER'] + filename, index=False)

    return render_template(
        'timeline.html',
        term=term,
        reddit_or_twitter=reddit_or_twitter,
        interval=interval,
        since=since,
        until=until,
        filename=filename,
        do_changepoint=do_changepoint,
        changepoint=changepoint
    )


@APP.route('/timeline/chart', methods=['GET'])
def _timeline_chart() -> str:
    """Serve the chart."""
    with open(APP.config['TMP_FOLDER'] + 'chart.json', 'r') as chart_json_in:
        chart_json = json.load(chart_json_in)

    return chart_json


@APP.route('/timeline/download/<filename>')
def _timeline_download(filename: str) -> Response:
    """Download the csv file."""
    # LOG.info(type(send_from_directory(APP.config['DOWNLOAD_FOLDER'],
    #                                   filename, as_attachment=True)))
    return send_from_directory(APP.config['DOWNLOAD_FOLDER'], filename, as_attachment=True)


########################################
#              describe           ######
########################################
@APP.route('/describe', methods=['POST'])
def _describe() -> str:
    """Return the describe."""
    # TODO: get arguments from user (flask.request)
    # TODO: pass those arguments into descriptions()
    descriptions = describe()
    return render_template('describe.html', descriptions=descriptions)


########################################
#             wordcloud           ######
########################################
@APP.route('/wordcloud')
def _wordcloud() -> str:
    """Return the wordcloud."""
    # TODO: in all honesty this will probably be mostly js, not python.
    return wordcloud()


########################################
#           related hashtags       ###
########################################
@APP.route('/hashtags', methods=['POST'])
def _hashtags() -> str:
    """Render the hashtags template."""
    term = request.values['term'] or 'tornado'
    limit = request.values['limit'] or 1000
    num_of_hashtags_graphed = request.values['num_of_hashtags_graphed'] or 15

    dat, chart = hashtags(term, limit, num_of_hashtags_graphed)

    with open(APP.config['TMP_FOLDER'] + 'chart.json', 'w') as chart_json_out:
        json.dump(chart.to_json(), chart_json_out)

    filename = f'{term}-hashtags.csv'
    dat.to_csv(APP.config['DOWNLOAD_FOLDER'] + filename)

    return render_template(
        'hashtags.html',
        term=term,
        limit=limit,
        filename=filename,
        num_of_hashtags_graphed=num_of_hashtags_graphed
    )


@APP.route('/hashtags/chart', methods=['GET'])
def _hashtags_chart() -> str:
    """Serve the chart."""
    with open(APP.config['TMP_FOLDER'] + 'chart.json', 'r') as chart_json_in:
        chart_json = json.load(chart_json_in)

    return chart_json
