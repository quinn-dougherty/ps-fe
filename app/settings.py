#!/usr/bin/env python
"""Settings."""

import os
from dotenv import load_dotenv  # type: ignore
load_dotenv()

REDDIT_COMMENT_SEARCH = os.getenv('REDDIT_COMMENT_SEARCH')
TWITTER_VERIFIED_SEARCH = os.getenv('TWITTER_VERIFIED_SEARCH')
MAPPING = os.getenv('MAPPING')
