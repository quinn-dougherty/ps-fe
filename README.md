# Frontends for pushshift. Inspired by the [slackbot](https://pushshift.io/slack-install/)

## the app is up [here](https://ps-fe.herokuapp.com)

## to run it locally: 

To run it locally, you can use either docker-compose or pipenv. The docker-compose version is 
optimized for easy deployment, while pipenv will give you faster iteration. 

### clone it and cd into it. have either docker & docker-compose or pipenv installed. 

### obtain the `.env` file from Slack.
When you download it, please ensure that windows or osx doesn't automatically change its 
name to `env`. The correct filename is `.env`. Put it in the base level of the project directory. 

### to use docker-compose: 
- `docker-compose up`
- when you make changes run `docker-compose up --build` to rebuild. 

**Important**: if you comment out `gunicorn` and use `flask run` in the Dockerfile, please switch
it back to gunicorn before pushing to github! 

### to use pipenv: 
- `pipenv shell` 
- `pipenv install`
- `export FLASK_APP=app:APP`
- `export FLASK_DEBUG=1`
- `flask run` 

Pythons 3.7 and 3.8 are the only ones that are verified. Feel free to experiment with earlier Python3s, 
by changing the last line of the Pipfile. 


## adding dependencies
1. Use pipenv to install a module
```
pipenv install flask-cors
```

2. Add the module to `requirements.txt` as well
```
./requirements.txt
 # web
 flask
+flask-cors
 requests
 gunicorn
 python-dotenv
```

3. Tell the type-checker to ignore your import (if necessary)
```python
from flask_cors import CORS  # type: ignore
```

4. Rebuild the container
```
docker-compose build --no-cache
```
