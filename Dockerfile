FROM python:3.8-slim-buster

WORKDIR /code
ENV FLASK_APP app:APP
ENV FLASK_RUN_HOST 0.0.0.0

# needed for the numpy behind pandas
RUN apt-get update -qqy && apt-get install -qqy libopenblas-dev gfortran

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

EXPOSE $PORT

COPY . .

# simulates the job named lint in the CI. Can be commented out for faster iteration. 
RUN mypy app \
  && pylint app --disable=fixme --disable=too-many-arguments \
  && pycodestyle app --max-line-length=120 \
  && pydocstyle app

# simulates the job named test in the CI. Can be commented out for faster iteration.
# MUST be commented out for heroku build because runtime config vars are not available at build.
# RUN nose2 -v

# WEB SERVER
# flask run is a development server, it can be advantageous because
# its logging is more verbose.
CMD gunicorn app:APP --bind $FLASK_RUN_HOST:$PORT
# CMD FLASK_DEBUG=1 flask run
